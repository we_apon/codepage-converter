﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;


namespace Codepage_Converter
{
    class Program
    {
        static void Main(string[] args) {
            Console.WriteLine("\tUsage: \n{0} [{1}] [{2}] [{3}] [{4}]\n{5}", 
                Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName),
                @"C:\Path\To\Root\Folder",
                "FileMask*.cs",
                "DestinationEncoding (like UTF-8)",
                "SourceEncoding (like 1251)",
                "\n\tDefault: \nCurrent working directory, \"*.cs\" UTF-8 and current system ansi codepage (" + Encoding.Default.CodePage + ")\n\n");

            var path = args.Length > 0 ? args[0] : AppDomain.CurrentDomain.BaseDirectory;
            var pattern = args.Length > 1 ? args[1] : "*.cs";
            var destinationEncoding = args.Length > 2 ? Encoding.GetEncoding(args[2]) : Encoding.UTF8;
            var sourceEncoding = args.Length > 3 ? Encoding.GetEncoding(args[3]) : Encoding.Default;


            var watch = new Stopwatch();
            watch.Start();
            foreach (var filePath in Directory.GetFiles(path, pattern, SearchOption.AllDirectories)) {
                var text = File.ReadAllText(filePath, sourceEncoding);
                File.WriteAllText(filePath, text, destinationEncoding);
            }

            Console.WriteLine("Finished in {0}", watch.Elapsed);
        }
    }
}
